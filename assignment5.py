#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

id_list = []

@app.route('/book/JSON/')
def bookJSON():
    return jsonify(books) 

@app.route('/')
def home():
    return render_template('showBook.html', books=books)

@app.route('/book/')
def showBook():
    return render_template('showBook.html', books=books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        new_book = {'title': request.form['name'], 'id': str(id_list.pop())}
        books.append(new_book)
        return redirect(url_for('home'))
    return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'POST':
        getBook(book_id)['title'] = request.form['name']
        return redirect(url_for('home'))
    else:
        return render_template('editBook.html', book=getBook(book_id))
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        books.remove(getBook(book_id))
        id_list.append(book_id)
        return redirect(url_for('home'))
    return render_template('deleteBook.html', book=getBook(book_id))

def getBook(book_id):
    for book in books:
        if book['id'] == str(book_id):
            return book
    return {}

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

